package contactproject;




import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author informatics
 */
public class Data {

    static ArrayList<Person> userList = new ArrayList<>();
    static Scanner kb = new Scanner(System.in);
    private static int id;
    private static String username;
    private static String password;
    private static String firstname;
    private static String lastname;
    private static int age;

    static void load() {
        userList.add(new Person(1,"user1", "password1", "name1", "surname1", 20));
        userList.add(new Person(2,"user2", "password2", "name2", "surname2", 20));
        userList.add(new Person(3,"user3", "password3", "name3", "surname3", 20));
        userList.add(new Person(4,"user4", "password4", "name4", "surname4", 20));
        userList.add(new Person(5,"user5", "password5", "name5", "surname5", 20));
    }

    static boolean isUserPasswordCorrect(String username, String password) {
        if (username.equals("admin") && password.equals("admin")) {
            return true;
        }

        for (Person p : userList) {
            if (p.getUsername().equals(username)) {
                if (p.getPassword().equals(password)) {
                    return true;
                }
            }
        }
        return false;
    }

    static void add() {
        inputUsername();
        System.out.print("Password:");
        password = kb.nextLine();
        System.out.print("Firstname:");
        firstname = kb.nextLine();
        System.out.print("Lastname:");
        lastname = kb.nextLine();
        while (true) {
            System.out.print("Age:");
            age = kb.nextInt();
            CheckAge(age);
            break;
        }
        userList.add(new Person(id,username, password, firstname, lastname, age));

    }

    private static void inputUsername() {
        while (true) {
            System.out.print("Username:");
            username = kb.nextLine();
            if (CheckUsername(username)) {
                break;
            }
        }
    }

    static void print() {
        for (Person p : userList) {
            System.out.println(p);
        }
    }

    private static boolean CheckUsername(String username) {
        for (Person p : userList) {
            if (p.getUsername().equals(username)) {
                System.out.println("Username Do not repeat.");
                return false;
            }
        }
        return true;
    }

    private static boolean CheckAge(int age) {
        return true;
    }

}
