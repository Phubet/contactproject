/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contactproject;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author informatics
 */
public class UserTableModel extends AbstractTableModel {

    String[] columnNames = {"id", "username", "name", "surname"};
    ArrayList<Person> userList = Data.userList;

    public UserTableModel() {

    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Person person = userList.get(rowIndex);
        if (person == null) {
            return "";
        }
        switch (columnIndex) {
            case 0:
                return person.getId();
            case 1:
                return person.getUsername();
            case 2:
                return person.getFristname();
            case 3:
                return person.getLastname();
        }
        return "";
    }

}
