package contactproject;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author informatics
 */
public class Person implements Serializable {

    private int id;
    private String username;
    private String password;
    private String fristname;
    private String lastname;
    private int age;

    public Person() {
    }

    public Person(int id,String username, String password, String fristname, String lastname, int age) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.fristname = fristname;
        this.lastname = lastname;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFristname() {
        return fristname;
    }

    public void setFristname(String fristname) {
        this.fristname = fristname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" + "username=" + username + ", password=" + password + ", fristname=" + fristname + ", lastname=" + lastname + ", age=" + age + '}';
    }
}
